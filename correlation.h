//
// Created by Marko on 18.09.2022.
//

#ifndef LAB1_OOP_CORRELATION_H
#define LAB1_OOP_CORRELATION_H

#include <vector>

double correlation();
std::vector<int> correlationDstrb();

#endif //LAB1_OOP_CORRELATION_H
