//
// Created by Marko on 18.09.2022.
//
#include "inversecong.h"

#include <vector>

#include "generalstaff.h"


int xn = 1;
int x_inv = 1;

int inverseCongInt(){
    while (((xn * x_inv) % m) != 1){
        x_inv++;
    }

    xn = (a * x_inv + c) % m;
    x_inv = 1;

    return xn;
}

std::vector<int> inverseCongDstrb(){

    std::vector<int> f(10, 0);

    double u;

    for (int i = 0; i < 10000; i++) {
        u = getURand(inverseCongInt());

        f[(int) (u * 10)]++;
    }
    return f;
}


