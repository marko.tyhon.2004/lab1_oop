//
// Created by Marko on 20.09.2022.
//

#ifndef LAB1_OOP_GENERALSTAFF_H
#define LAB1_OOP_GENERALSTAFF_H

const int m = 9001;
const int a = 7;
const int c = 18;
const int d = 5;
const int miu = 50;
const double e = 2.71828182846;
const double pi = 3.14159265359;

double getURand(int someIntRand);
void uniformDstrbs(int method_number);
void normalDstrbs(int method_number);
void elseDstrbs(int method_number);

#endif //LAB1_OOP_GENERALSTAFF_H
