//
// Created by Marko on 20.09.2022.
//

#include "generalstaff.h"

#include <iostream>
#include <vector>
#include <iomanip>

#include "linearcong.h"
#include "quadcong.h"
#include "fibonaccinums.h"
#include "inversecong.h"
#include "thejoin.h"
#include "therule3sigma.h"
#include "polarcoord.h"
#include "correlation.h"
#include "thelogarithm.h"
#include "ahrensmethod.h"


double getURand(int someIntRand){
    return (double)someIntRand/m;
}

void uniformDstrbs(int method_number){
    std::vector<int> f;

    switch (method_number) {
        case 1 :
            f = linearCongDstrb();
            break;
        case 2 :
            f = quadCongDstrb();
            break;
        case 3 :
            f = fibonacciNumsDstrb();
            break;
        case 4 :
            f = inverseCongDstrb();
        case 5 :
            f = theJoinDstrb();
            break;
        default: std::cout << "OMG! Smt went wrong. I'm so sorry..." << std::endl;
    }

    double h = 0.0;
    std::cout << "Interval" << "     " << "Frequency " << std::endl;

    for (int k : f) {
        std::cout << std::fixed << std::setprecision(1);
        std::cout << "[" << h << ";" << h + 0.1 << "]";
        std::cout << std::fixed << std::setprecision(4);
        std::cout << "    " << (double)k/10000 << std::endl;
        h += 0.1;
    }
}


void normalDstrbs(int method_number){
    std::vector<int> f;

    switch (method_number) {
        case 6 :
            f = theRule3SigmaDstrb();
            break;
        case 7 :
            f = polarCoordDstrb();
            break;
        case 8 :
            f = correlationDstrb();
            break;
        default: std::cout << "OMG! Smt went wrong. I'm so sorry..." << method_number << std::endl;
    }
    double h = -3;
    std::cout << "Interval" << "         " << "Frequency " << std::endl;

    for (int k:f) {
        std::cout << std::fixed << std::setprecision(2);
        std::cout << "[" << h << ";" << h + 0.6 << "]";
        std::cout << std::fixed << std::setprecision(4);
        std::cout << "    " << (double)k / 10000 << std::endl;
        h += 0.6;
    }
}


void elseDstrbs(int method_number){
    std::vector<int> f;

    switch (method_number) {
        case 9 :
            f = theLogarithmDstrb();
            break;
        case 10 :
            f = ahrensMethodDstrb();
            break;
        default: std::cout << "OMG! Smt went wrong. I'm so sorry..." << std::endl;
    }

    std::cout << "Interval" << "     " << "Frequency " << std::endl;
    int h = 0;
    std::cout << std::fixed << std::setprecision(4);
    for (int k:f) {
        std::cout << "[" << h*10 << ";" << (h + 1)*10 << "]";
        std::cout << "       " << (double) k / 10000 << std::endl;
        h += 1;
    }
}
