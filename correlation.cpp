//
// Created by Marko on 18.09.2022.
//

#include "correlation.h"

#include <vector>
#include <cmath>

#include "generalstaff.h"
#include "linearcong.h"
#include "fibonaccinums.h"

double u, v, x, x_quad;

double correlation(){
    do{
        u = getURand(linearCongInt());
        v = getURand(fibonacciNumsInt());

        x = sqrt(8 / e)  * (v - 0.5) / u;

        x_quad = x*x;
    }while((x_quad > 5 - 4*std::exp(0.25)*u)
        && (x_quad >= (4 * std::exp(-1.35)) / u + 1.4)
        && (x_quad > -4*log(u)));
    return x;
}

std::vector<int> correlationDstrb(){

    std::vector<int> f(10, 0);

    double _u;

    for (int i = 0; i < 10000; i++) {
        _u = correlation();

        if ((-3 <= _u)&&(_u<=3)){

            f[(int)((_u+3)*5/3)]++;
        }
    }
    return f;
}