//
// Created by Marko on 18.09.2022.
//

#ifndef LAB1_OOP_QUADCONG_H
#define LAB1_OOP_QUADCONG_H

#include <vector>

int quadCongInt();
std::vector<int> quadCongDstrb();

#endif //LAB1_OOP_QUADCONG_H
