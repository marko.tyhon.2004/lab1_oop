//
// Created by Marko on 19.09.2022.
//

#ifndef LAB1_OOP_THELOGARITHM_H
#define LAB1_OOP_THELOGARITHM_H

#include <vector>

double theLogarithm();
std::vector<int> theLogarithmDstrb();

#endif //LAB1_OOP_THELOGARITHM_H
