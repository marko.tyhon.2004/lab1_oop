//
// Created by Marko on 18.09.2022.
//

#ifndef LAB1_OOP_LINEARCONG_H
#define LAB1_OOP_LINEARCONG_H

#include <vector>

int linearCongInt();
std::vector<int> linearCongDstrb();

#endif //LAB1_OOP_LINEARCONG_H
