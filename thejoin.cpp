//
// Created by Marko on 18.09.2022.
//

#include "thejoin.h"

#include <vector>

#include "generalstaff.h"
#include "fibonaccinums.h"
#include "inversecong.h"


int x_join;

int theJoinInt(){
    x_join = (fibonacciNumsInt() - inverseCongInt() + m) % m;
    return x_join;
}


std::vector<int> theJoinDstrb(){

    std::vector<int> f(10, 0);

    double u;

    for (int i = 0; i < 10000; i++) {
        u = getURand(theJoinInt());

        f[(int) (u * 10)]++;
    }
    return f;
}


