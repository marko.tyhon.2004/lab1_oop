//
// Created by Marko on 19.09.2022.
//

#include "thelogarithm.h"

#include <cmath>
#include <vector>

#include "generalstaff.h"
#include "thejoin.h"

double x_log;

double theLogarithm(){
    do{
        x_log = -miu * log(getURand(theJoinInt()));
    } while ((x_log <= 0) && (x_log >= 100));
    return x_log;
}

std::vector<int> theLogarithmDstrb() {

    std::vector<int> f(10, 0);
    double u;
    for (int i = 0; i < 10000; i++) {
        u = theLogarithm();
        if ((0 <= u) && (u <= 100)) {
            f[(int) ((u) / 10)]++;
        }
    }
    return f;
}
