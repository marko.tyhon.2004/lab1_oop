//
// Created by Marko on 19.09.2022.
//

#include "ahrensmethod.h"

#include <cmath>
#include <vector>

#include "generalstaff.h"
#include "quadcong.h"
#include "linearcong.h"

using namespace std;

double y_ahres, x_ahres;


double ahrensMethod(){
    do{

    y_ahres = tan(pi * getURand(linearCongInt()));
    x_ahres = sqrt(2*a) * y_ahres + a - 1;

    } while ((x_ahres <= 0) &&
    (getURand(quadCongInt()) > (1 + y_ahres*y_ahres) * std::exp((a-1) *
    log(x_ahres /(a-1)) - sqrt(2*a-1) * y_ahres)));

    return x_ahres;
}


std::vector<int> ahrensMethodDstrb() {

    std::vector<int> f(10, 0);
    double u;
    for (int i = 0; i < 10000; i++) {
        u = ahrensMethod();
        if ((0 <= u) && (u <= 100)) {
            f[(int) ((u) / 10)]++;
        }
    }
    return f;
}

