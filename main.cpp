#include <iostream>

#include "generalstaff.h"

int getMethodNumber(const std::string& text){
    int curr_input = 0;
    do {
        std::cout << text << std::endl;
        std::cin >> curr_input;
        if ((curr_input <= 0) || (10 < curr_input)) {
            std::cout << "Method with number " << curr_input << " doesn't exist.\n" << std::endl;
            curr_input = -1;
        }
    }while (curr_input == -1);

    return curr_input;
}

int main(){
    std::string text;
    text = "Methods:\n\t1 - Linear congruent method."
           "\n\t2 - Quadratic congruent method.\n\t3 - Fibonacci numbers.\n\t4 - Inverse congruent sequence"
           "\n\t5 - Method of association.\n\t6 - The rule of \"3 sigma\".\n\t7 - The method of polar coordinates."
           "\n\t8 - Method of correlations. \n\t9 - The logarithm method for generating an indicative distribution."
           "\n\t10 - Ahrens' method for generating a gamma distribution of order a > 1."
           "\nEnter number of method which you want to try:";

    int method_number = getMethodNumber(text);

    if (method_number <= 5){
    uniformDstrbs(method_number);

    } else if(method_number <= 8){
        normalDstrbs(method_number);

    } else{
      elseDstrbs(method_number);
    }

}



