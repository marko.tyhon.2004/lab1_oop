//
// Created by Marko on 18.09.2022.
//

#ifndef LAB1_OOP_INVERSECONG_H
#define LAB1_OOP_INVERSECONG_H

#include <vector>

int inverseCongInt();
std::vector<int> inverseCongDstrb();

#endif //LAB1_OOP_INVERSECONG_H
