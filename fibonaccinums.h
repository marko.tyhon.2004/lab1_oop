//
// Created by Marko on 18.09.2022.
//

#ifndef LAB1_OOP_FIBONACCINUMS_H
#define LAB1_OOP_FIBONACCINUMS_H

#include <vector>

int fibonacciNumsInt();
std::vector<int> fibonacciNumsDstrb();

#endif //LAB1_OOP_FIBONACCINUMS_H
