//
// Created by Marko on 19.09.2022.
//

#ifndef LAB1_OOP_AHRENSMETHOD_H
#define LAB1_OOP_AHRENSMETHOD_H

#include <vector>

double ahrensMethod();
std::vector<int> ahrensMethodDstrb();

#endif //LAB1_OOP_AHRENSMETHOD_H
