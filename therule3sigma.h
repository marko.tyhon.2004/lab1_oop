//
// Created by Marko on 18.09.2022.
//

#ifndef LAB1_OOP_THERULE3SIGMA_H
#define LAB1_OOP_THERULE3SIGMA_H

#include <vector>

double theRule3Sigma();
double sum0_12();
std::vector<int> theRule3SigmaDstrb();

#endif //LAB1_OOP_THERULE3SIGMA_H
