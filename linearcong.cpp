//
// Created by Marko on 18.09.2022.
//
#include "linearcong.h"

#include <vector>

#include "generalstaff.h"

int x_linear;

int linearCongInt(){
    x_linear = (x_linear * a + c) % m;
    return x_linear;
}

std::vector<int> linearCongDstrb() {

    std::vector<int> f(10, 0);

    double u;


    for (int i = 0; i < 10000; i++) {
        u = getURand(linearCongInt());

        f[(int)(u*10)]++;
    }
    return f;
}




