//
// Created by Marko on 18.09.2022.
//

#include "quadcong.h"

#include <vector>

#include "generalstaff.h"

int xn_quad;

int quadCongInt(){
    xn_quad = (xn_quad*xn_quad * d + xn_quad * a + c) % m;
    return xn_quad;
}
std::vector<int> quadCongDstrb(){

    std::vector<int> f(10, 0);

    double u;

    for (int i = 0; i < 10000; i++) {
        u = getURand(quadCongInt());

        f[(int) (u * 10)]++;
    }
    return f;
}

