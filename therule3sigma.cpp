//
// Created by Marko on 18.09.2022.
//

#include "therule3sigma.h"

#include <vector>

#include "generalstaff.h"
#include "inversecong.h"

double x_sigma, sum = 0;

double sum0_12(){
    sum = 0;
    for (int i = 0; i < 12; i++){
        sum += getURand(inverseCongInt());
    }
    return sum;
}

double theRule3Sigma(){
    do{
        x_sigma = (sum0_12() - 6);
    } while ((-3 >= x_sigma) || (x_sigma >= 3));
    return x_sigma;
}

std::vector<int> theRule3SigmaDstrb() {

    std::vector<int> f(10, 0);

    double u;

    for (int i = 0; i < 10000; i++) {
        u = theRule3Sigma();

        if ((-3 <= u)&&(u<=3)){

        f[(int)((u+3)*5/3)]++;
        }
    }
    return f;
}


