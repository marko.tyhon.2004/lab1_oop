cmake_minimum_required(VERSION 3.20)
project(Lab1_OOP)

set(CMAKE_CXX_STANDARD 14)

set(Headers generalstaff.h linearcong.h quadcong.h fibonaccinums.h inversecong.h thejoin.h therule3sigma.h
        polarcoord.h correlation.h thelogarithm.h ahrensmethod.h)

add_executable(Lab1_OOP main.cpp generalstaff.cpp linearcong.cpp quadcong.cpp fibonaccinums.cpp inversecong.cpp thejoin.cpp
        therule3sigma.cpp polarcoord.cpp correlation.cpp thelogarithm.cpp ahrensmethod.cpp)
