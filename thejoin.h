//
// Created by Marko on 18.09.2022.
//

#ifndef LAB1_OOP_THEJOIN_H
#define LAB1_OOP_THEJOIN_H

#include <vector>

int theJoinInt();
std::vector<int>  theJoinDstrb();

#endif //LAB1_OOP_THEJOIN_H
