//
// Created by Marko on 18.09.2022.
//
#include "fibonaccinums.h"

#include <iostream>
#include <vector>

#include "generalstaff.h"

int x_fib = 0;
int x_prev = 1;
int x_buf;

int fibonacciNumsInt(){
    x_buf = x_fib;
    x_fib = (x_fib + x_prev) % m;
    x_prev = x_buf;

    return x_fib;
};

std::vector<int> fibonacciNumsDstrb() {

    std::vector<int> f(10, 0);

    double u;

    for (int i = 0; i < 10000; i++) {
        u = getURand(fibonacciNumsInt());

        f[(int)(u*10)]++;
    }
    return f;
}