//
// Created by Marko on 18.09.2022.
//

#ifndef LAB1_OOP_POLARCOORD_H
#define LAB1_OOP_POLARCOORD_H

#include <vector>

double polarCoord();
std::vector<int> polarCoordDstrb();

#endif //LAB1_OOP_POLARCOORD_H
