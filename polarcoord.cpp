//
// Created by Marko on 18.09.2022.
//

#include "polarcoord.h"

#include <vector>
#include <cmath>

#include "generalstaff.h"
#include "fibonaccinums.h"

double x1 = 0;

double v1, v2, s, x2;

double polarCoord() {
    if (x1 == 0) {
        do{
            v1 = 2 * getURand(fibonacciNumsInt()) - 1;
            v2 = 2 * getURand(fibonacciNumsInt()) - 1;

            s = v1 * v1 + v2 * v2;
            } while (s>=1);

        x1 = v1 * sqrt((-2 * log(s)) / s);
        x2 = v2 * sqrt((-2 * log(s)) / s);
        return x1;
    }
    x1 = 0;
    return x2;
}

std::vector<int> polarCoordDstrb(){

    std::vector<int> f(10, 0);

    double u;

    for (int i = 0; i < 10000; i++) {
        u = polarCoord();

        if ((-3 <= u)&&(u<=3)){

            f[(int)((u+3)*5/3)]++;
        }
    }
    return f;
}